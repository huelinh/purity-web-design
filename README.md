# purity-web-design
- This is project design for Purity website.
- Apply Technical:
    + BEM
    + Flex box
    + Bootstrap
    + SCSS
- Target: Complete the landing page template as close to the request as possible.
- How to run?
    + clone project to your computer:
    + Open Terminal.
    + Change the current working directory to the location where you want the cloned directory to be made.
    + Type git clone, and then paste the URL:
        $ git clone https://gitlab.com/huelinh/purity-web-design.git
    + Press Enter. Your local clone will be created.
    + Open the folder you just clone -> open folder html -> right click on the file purity.html -> choose open with browser
- How to develop?
 + If you want to change html file: open folder html -> choose purity.html
 + If your want to change style for web:
    1. Open folder 'SCSS'
    2. Modify file inside
    2. Open Terminal -> Change the current working directory to the folder 'SCSS'.
        Type: sass <file_modify>.scss ../css/<file_covert>.css
