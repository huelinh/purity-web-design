$(document).ready(function () {
	let vw = window.screen.width;
	window.addEventListener("resize", function () {
		vw = window.screen.width;
	});
	const slideToShow2 = 992 <= vw ? 3 : 991 >= vw && vw >= 768 ? 2 : 1;
	console.log("vw:", vw);
	console.log("slideToShow2:", slideToShow2, "check:", 991 >= vw && vw >= 768);
	$(".sliderSlick").slick({
		focusOnSelect: false,
		slidesToShow: 6,
		dots: true,
		infinite: true,
		speed: 300,
	});

	$(".slider2Slick").slick({
		focusOnSelect: false,
		slidesToShow: slideToShow2,
		slidesToScroll: 3,
		dots: true,
		infinite: true,
		speed: 300,
	});
	var focusOnSelect = false;
	$(".js-setoption").on("click", function () {
		focusOnSelect = !focusOnSelect;
		$(".sliderSlick").slick(
			"slickSetOption",
			{
				focusOnSelect: focusOnSelect,
			},
			true
		);
		if (focusOnSelect === true) {
			$(this).text("無効化");
		} else {
			$(this).text("有効化");
		}
	});
});
